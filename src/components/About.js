import React from 'react';
import '../styles/About.css';
import me from '../assets/me.jpg';

function About() {
  return (
    <div className="about">
      <h2>About me</h2>
      <div className="content">
        <img src={me} className="image"></img>
        <div>
          <p>Hobbies: Basketball, fitness</p>
          <p>Uni: FIPU</p>
        </div>
      </div>
    </div>
  );
}

export default About;
