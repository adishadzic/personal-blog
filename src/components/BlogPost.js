import React from 'react';
import '../styles/BlogPost.css';
import { blogs } from './blogs';
import cat from '../assets/cat.jpeg';

function BlogPost({ blogTitle, blogText }) {
  return (
    <div className="card">
      <img src={cat} alt="cat" style={{ width: '100%' }}></img>
      <div className="container">
        <h4>{blogTitle}</h4>
        <p>{blogText}</p>
      </div>
    </div>
  );
}

export default BlogPost;
