import React from 'react';
import '../styles/Favorites.css';

function Favorites() {
  return (
    <div className="favorites">
      <h2>Favorites</h2>
      <p>
        Favorite team: <a href="https://www.nba.com/suns/">Phoenix Suns</a>
      </p>
      <p>
        Favorite food place:{' '}
        <a href="https://www.tripadvisor.com/Restaurant_Review-g295373-d17666155-Reviews-KebaBoss-Pula_Istria.html">
          Kebaboss
        </a>
      </p>
    </div>
  );
}

export default Favorites;
