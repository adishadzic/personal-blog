import React from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import { routes } from '../routes';
import About from './About';
import Favorites from './Favorites';
import Home from './Home';
import '../styles/Header.css';

function Header() {
  return (
    <div className="header">
      <Router>
        <nav className="header_nav">
          <ul>
            <li className="header_logo">
              <h4>Adis Hadzic</h4>
            </li>
            {routes.map((route, index) => {
              return (
                <li key={index}>
                  <Link to={route.path}>{route.pathLabel}</Link>
                </li>
              );
            })}
          </ul>
        </nav>
        <Switch>
          <Route path="/about">
            <About />
          </Route>
          <Route path="/favorites">
            <Favorites />
          </Route>
          <Route path="/">
            <Home />
          </Route>
        </Switch>
      </Router>
    </div>
  );
}

export default Header;
