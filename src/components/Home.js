import React from 'react';
import BlogPost from './BlogPost';
import '../styles/Home.css';

function Home() {
  return (
    <div className="home_blogs">
      <BlogPost blogTitle="Blog 1" blogText="blablablabla" />
      <BlogPost blogTitle="Blog 2" blogText="blablablabla" />
      <BlogPost blogTitle="Blog 3" blogText="blablablabla" />
      <BlogPost blogTitle="Blog 4" blogText="blablablabla" />
      <BlogPost blogTitle="Blog 5" blogText="blablablabla" />
    </div>
  );
}

export default Home;
