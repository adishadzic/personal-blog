import Home from './components/Home';
import About from './components/About';
import Favorites from './components/Favorites';

export const routes = [
  {
    path: '/',
    pathLabel: 'Home',
    RouteComponent: Home,
  },
  {
    path: '/about',
    pathLabel: 'About',
    RouteComponent: About,
  },
  {
    path: '/favorites',
    pathLabel: 'Favorites',
    RouteComponent: Favorites,
  },
];
